An collapse command summarizes data using aggregation functions applied to data
that may be grouped by one or more variables. The resulting summary data is
represented in a new dataset.
